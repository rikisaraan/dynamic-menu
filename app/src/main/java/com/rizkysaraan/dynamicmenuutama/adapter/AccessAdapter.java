package com.rizkysaraan.dynamicmenuutama.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rizkysaraan.dynamicmenuutama.R;
import com.rizkysaraan.dynamicmenuutama.domain.Access;

import java.text.NumberFormat;
import java.util.List;

import static com.rizkysaraan.dynamicmenuutama.utils.Constant.URLImage;

public class AccessAdapter extends RecyclerView.Adapter<AccessAdapter.AccessHolder> {
    private List<Access> listAccess;
    private OnCardListener mOnCardListener;
    private Context context;

    public AccessAdapter(Context context, List<Access> listAccess, OnCardListener mOnCardListener) {
        this.context = context;
        this.listAccess = listAccess;
        this.mOnCardListener = mOnCardListener;
    }

    @NonNull
    @Override
    public AccessHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_menu, viewGroup, false);
        return new AccessHolder(view,mOnCardListener);
    }

    @Override
    public void onBindViewHolder(final AccessHolder holder, int position) {
        Access access =listAccess.get(position);
        holder.tv_title.setText(access.getLabel());
        Glide.with(context)
                .load(URLImage+access.getImage())
                .apply(new RequestOptions().override(80, 80))
                .into(holder.img_icon);
    }

    @Override
    public int getItemCount() {
        return listAccess.size();
    }

    class AccessHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_title;
        ImageView img_icon;
        CardView card_view;
        OnCardListener onCardListener;

        AccessHolder(View itemView,OnCardListener onCardListener) {
            super(itemView);
            tv_title= itemView.findViewById(R.id.tv_title);
            img_icon= itemView.findViewById(R.id.img_icon);

            card_view = itemView.findViewById(R.id.card_view);

            this.onCardListener = onCardListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onCardListener.onCardClick(getAdapterPosition());
        }
    }

    public interface OnCardListener {
        void onCardClick(int position);
    }
}
