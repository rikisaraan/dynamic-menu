package com.rizkysaraan.dynamicmenuutama.utils;

public class Constant {
    public static final String URL = "http://192.168.43.210/api-dynamicaccess/index.php";
    public static final String URLImage = "http://192.168.43.210/api-dynamicaccess/public/icon/";
    public static final String EXTRA_ITEM = "extra_item";
    public static final String TEST_TYPE = "test";
}
