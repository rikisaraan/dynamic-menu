package com.rizkysaraan.dynamicmenuutama;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.rizkysaraan.dynamicmenuutama.adapter.AccessAdapter;
import com.rizkysaraan.dynamicmenuutama.domain.Access;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.rizkysaraan.dynamicmenuutama.utils.Constant.EXTRA_ITEM;
import static com.rizkysaraan.dynamicmenuutama.utils.Constant.URL;

public class MainActivity extends AppCompatActivity implements AccessAdapter.OnCardListener {
    private RecyclerView rvAccess;
    private AccessAdapter accessAdapter;
    private List<Access> listAccess;
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvAccess = findViewById(R.id.rv_access);
        rvAccess.setHasFixedSize(true);

        listAccess = new ArrayList<>();

        rvAccess.setLayoutManager(new GridLayoutManager(this, 3));

        accessAdapter = new AccessAdapter(getApplicationContext(), listAccess, this);
        rvAccess.setAdapter(accessAdapter);

        getListAccess();
    }

    @Override
    public void onCardClick(int position) {
        Intent moveWithObjectIntent;
        String name = listAccess.get(position).getName();

        //moveWithObjectIntent = new Intent(this, Class.forName("DetailActivity"));
        //moveWithObjectIntent.putExtra(EXTRA_ITEM, listAccess.get(position));
        //startActivity(moveWithObjectIntent);

        String activityString = "com.rizkysaraan.dynamicmenuutama."+name;
        Intent intent = new Intent().setClassName(MainActivity.this,activityString);
        startActivity(intent);

    }

    public void getListAccess() {

        final String urlAccess = URL + "/Access";

        JsonArrayRequest jArr = new JsonArrayRequest(urlAccess, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                listAccess.clear();
                if (response.length() < 1) {
                    Log.e(TAG, "Data tidak ada");
                } else {
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject obj = response.getJSONObject(i);

                            Access item = new Access();

                            item.setName(obj.getString("access_name"));
                            item.setImage(obj.getString("access_icon"));
                            item.setLabel(obj.getString("access_label"));

                            // menambah item ke array
                            listAccess.add(item);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                accessAdapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error.Response" + error.getMessage());

            }
        });
        // menambah request ke request queue
        jArr.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );
        AppController.getInstance().addToRequestQueue(jArr);
    }
}